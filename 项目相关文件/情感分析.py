import snownlp
import re

good_in_seven = 0
bad_in_seven = 0
good_in_eight = 0
bad_in_eight = 0

with open('情绪对比.txt', 'r', encoding='utf-8') as fp:

    for i in fp:
        neirong = re.sub(r'\s.*1', '',  str(i))

        sentence = snownlp.SnowNLP(neirong)
        felling = sentence.sentiments
        if felling > 0.5 and 'Sat Aug 07' in i:
            good_in_seven += 1
        elif felling < 0.5 and 'Sat Aug 07' in i:
            bad_in_seven += 1
        elif felling > 0.8 and 'Sun Aug 08' in i:
            good_in_eight += 1
        elif felling < 0.6 and 'Sun Aug 08' in i:
            bad_in_eight += 1

# print(good_in_seven, bad_in_seven)
# print(good_in_eight, bad_in_eight)

good_seven_per = good_in_seven/(good_in_seven + bad_in_seven)
bad_seven_per = bad_in_seven/(good_in_seven + bad_in_seven)
good_eight_per = good_in_eight/(good_in_eight + bad_in_eight)
bad_eight_per = bad_in_eight/(good_in_eight + bad_in_eight)

print('七号持乐观态度的百分比为' + str(good_seven_per) + '\t消极态度的百分比为' + str(bad_seven_per))
print('八号持乐观态度的百分比为' + str(good_eight_per) + '\t消极态度的百分比为' + str(bad_eight_per))